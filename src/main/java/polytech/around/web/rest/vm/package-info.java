/**
 * View Models used by Spring MVC REST controllers.
 */
package polytech.around.web.rest.vm;
